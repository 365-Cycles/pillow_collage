import csv
import math
import re
import sys
from collections import namedtuple
from datetime import datetime
from io import BytesIO

import PIL
import requests
from PIL import Image
from PySide2.QtCore import QThread, QSettings
from PySide2.QtCore import Signal, Slot
from PySide2.QtGui import QFont
from PySide2.QtWidgets import QWidget, QLabel, QTextEdit, QLineEdit, QPushButton, QProgressBar, QGridLayout, \
    QFileDialog, QApplication

from pillow_collage.ca_api import CAApi
from pillow_collage.main import dynamic_collage


class CollageThread(QThread):
    prog_sig = Signal(int)
    txt_sig = Signal(str)
    alert_sig = Signal()

    def __init__(self, parent=None, skus=(), o="", long_dim=1000):
        super(CollageThread, self).__init__(parent)
        self.skus = skus
        self.o = o
        self.l_dim = long_dim

    def run(self):
        img_url = {}
        f_url = "ITEMIMAGEURL1=https://d3d71ba2asa5oz.cloudfront.net/12011197/images/{}-{}.jpg"
        today = datetime.now().strftime("%Y-%m-%d")
        self.txt_sig.emit("Getting product info from CA.")
        with CAApi() as ca:
            for code, json in ca.get_products(self.skus):
                self.txt_sig.emit("... {}.".format(code))
                urls = []
                for prod in sorted(json.get('value', []), key=lambda x: x.get('Sku')):
                    if prod.get('Images', []):
                        urls.append(prod.get('Images', [])[0].get('Url'))
                    if len(urls) >= 4:
                        break
                img_url[code] = urls
        with open(self.o + '/parent_images_{}.csv'.format(today), 'wt') as o:
            idx = 1
            self.txt_sig.emit("Writing csv and collaging images.".format(today))
            writer = csv.writer(o)
            writer.writerow(["SKU", "Picture URLs"])
            for k, v in img_url.items():
                self.txt_sig.emit("... {}.".format(k))
                imgs = []
                for img in v:
                    r = requests.get(
                        img,
                        stream=True,
                        allow_redirects=True,
                        timeout=5
                    )
                    try:
                        imgs.append(Image.open(BytesIO(r.content)))
                    except PIL.UnidentifiedImageError as e:
                        self.txt_sig.emit("... Broken img on parent sku {}.".format(k))
                        continue
                if len(imgs) > 0:
                    for idx, i in enumerate(imgs):
                        imgs[idx] = i.resize(imgs[0].size, resample=Image.LANCZOS)
                    final = dynamic_collage(imgs)
                else:
                    final = imgs[0]
                if final.size[0] >= final.size[1]:
                    size = (self.l_dim, math.floor(self.l_dim / final.size[0] * final.size[1]))
                else:
                    size = (math.floor(self.l_dim / final.size[1] * final.size[0]), self.l_dim)
                if len(imgs) > 0:
                    final.resize(size, resample=Image.LANCZOS).save(self.o + "/{}-{}.jpg".format(k, today), format="JPEG")
                    final.save(self.o + "/{}-{}.jpg".format(k, today), format="JPEG")
                    writer.writerow([k, f_url.format(k, today)])
                self.prog_sig.emit(idx)
                idx += 1
        self.txt_sig.emit("Ding!")


class QBPToolsGui(QWidget):
    alert_signal = Signal()
    settings = QSettings(QSettings.UserScope, "365Cycles", "collage_tool")

    def __init__(self, args):
        QWidget.__init__(self)
        label_font = QFont("Helvetica", 20, QFont.Bold)
        # Defining the dimensions of the app.
        self.dim = namedtuple("dimension", ['x', 'y'])
        self.dim.x = 800
        self.dim.y = 600

        # Defining the grid size.
        self.grid = namedtuple("count", ['x', 'y', 'size'])
        self.grid.x = 20
        self.grid.size = self.dim.x // self.grid.x
        self.grid.y = self.dim.y // self.grid.size

        # Grid helpers
        self.bottom = self.grid.y - 1

        self.sku_label = QLabel("&Skus")
        self.sku_label.setFont(label_font)
        self.sku_list = QTextEdit()
        self.sku_list.setAcceptRichText(False)
        self.sku_label.setBuddy(self.sku_list)

        if len(args) > 1:
            with open(args[1], 'rt') as i:
                self.sku_list.setPlainText(i.read())

        self.op_label = QLabel("Options")
        self.op_label.setFont(label_font)
        self.dir_display = QLineEdit()
        self.dir_display.setReadOnly(True)
        self.output = QPushButton("&Output Location")
        self.log = QTextEdit()
        self.log.setAcceptRichText(False)
        self.log.setReadOnly(True)

        self.progress = QProgressBar()

        self.start_button = QPushButton("Start")
        self.start_button.setEnabled(False)

        # Column 0
        self.layout = QGridLayout()
        self.layout.addWidget(self.sku_label, 0, 0)
        self.layout.addWidget(self.sku_list, 1, 0, self.bottom - 2, 1)
        self.layout.addWidget(self.progress, self.bottom, 0, 1, 2)

        # Column 1
        self.layout.addWidget(self.op_label, 0, 1)
        self.layout.addWidget(self.dir_display, 1, 1)
        self.layout.addWidget(self.output, 2, 1)
        self.layout.addWidget(self.start_button, 3, 1)
        self.layout.addWidget(self.log, 4, 1, self.bottom - 5, -1)

        # Row stretching.
        self.layout.setRowStretch(0, 0)
        self.layout.setRowStretch(1, 2)
        self.layout.setRowStretch(2, 2)

        # Column stretching.
        self.layout.setColumnStretch(0, 1)
        self.layout.setColumnStretch(1, 1)

        # Setting the layout.
        self.setLayout(self.layout)

        # Adding space for the Q Thread.
        self.q_thread = None

        # Connecting the signal
        self.start_button.clicked.connect(self.get_data)
        self.output.clicked.connect(self.select_dir)

        # Storing useful things.
        self.target = None

    @Slot(int)
    def handle_prog(self, e):
        self.progress.setValue(e)

    @Slot(str)
    def handle_prog_txt(self, e):
        self.log.insertPlainText(e + '\n')

    @Slot()
    def handle_done_sig(self):
        self.alert_signal.emit()

    @Slot()
    def get_data(self):
        self.output.setEnabled(False)
        self.start_button.setEnabled(False)
        try:
            t = self.sku_list.toPlainText()
            if t:
                skus = re.split(r'\s|\t|,|\n', t)
            else:
                skus = []
            self.progress.setMaximum(len(skus))
            print(self.target, skus)
            self.q_thread = CollageThread(o=self.target, skus=skus)
            if not self.q_thread.isRunning():
                self.q_thread.prog_sig.connect(self.handle_prog)
                self.q_thread.txt_sig.connect(self.handle_prog_txt)
                self.q_thread.alert_sig.connect(self.handle_done_sig)
                self.q_thread.start()
        except Exception as e:
            self.log.insertPlainText(str(e) + '\n')
            raise e
        finally:
            self.start_button.setEnabled(True)
            self.progress.setValue(0)
            self.output.setEnabled(True)

    @Slot()
    def select_dir(self):
        dname = QFileDialog.getExistingDirectory(
            parent=self, caption='Choose Target Directory')
        if dname:
            self.target = dname
            self.dir_display.setText(dname)
            self.start_button.setEnabled(True)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = QBPToolsGui(sys.argv)
    widget.setFixedSize(widget.dim.x, widget.dim.y)
    widget.alert_signal.connect(app.beep)
    widget.alert_signal.connect(widget.raise_)
    widget.alert_signal.connect(widget.activateWindow)
    widget.show()
    sys.exit(app.exec_())
