from datetime import datetime

import requests

from pillow_collage.helpers import iter_slices, first_true


class CAApi(object):
    __ca_auth = {
        "auth":
        'a3YzZDV5YmpiNGF2enY1dnRlZzJqZDMzMmhodXVzMnY6WDhpRjJual9lVWFRU2Nzb01DUzktdw==',
        "refresh": 'rVGPi97cad4sBnM637kitJEtbKGa2HaIBoic83nqqQw'
    }
    acc_id = {
        "365 Cycles": "12011197"
    }
    __token = None
    __token_start = datetime.now()
    __token_expiry = None
    __test_mode = None
    __res = {
        "get_products": "Products",
        "get_attrs": "Products({})/Attributes"
    }
    __url = "https://api.channeladvisor.com/v1/"

    def __init__(self, test_mode=False):
        self.__test_mode = test_mode

    def __enter__(self):
        self.__get_token()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass

    def __get_token(self):
        """Gets a fresh CA oauth token.

        Raises:
            ChannelAdvisorTokenError -- Throws this error if the method fails to get the token.

        """
        url = "https://api.channeladvisor.com/oauth2/token"
        auth = ' '.join(["Basic", self.__ca_auth["auth"]])
        payload = "grant_type=refresh_token&refresh_token={}".format(
            self.__ca_auth["refresh"])
        headers = {
            'Authorization': auth,
            'Content-Type': "application/x-www-form-urlencoded",
            'cache-control': "no-cache"
        }

        r = requests.post(url, data=payload, headers=headers)
        if r.status_code == 200:
            self.__token = r.json()['access_token']
            self.__token_start = datetime.now()
            self.__token_expiry = r.json()['expires_in'] - 60
        else:
            raise ChannelAdvisorTokenError(
                "Failed to get CA API token, recieved http status code: " +
                str(r.status_code))

    def __check_token(self):
        """Simple method to refresh tokens if they need to be refreshed."""
        delta = datetime.now() - self.__token_start
        if delta.total_seconds() >= self.__token_expiry:
            self.__get_token()
        else:
            pass

    def get_products(self, codes=()):
        """Gets product data from CA."""
        self.__check_token()
        url = ''.join([self.__url, self.__res["get_products"]])
        for model_code in codes:
            fil = "Attributes/Any (c:c/Name eq 'Model Code' and c/Value eq '{}') and Sku ne '{}'".format(
                model_code,
                model_code
            )
            querystring = {
                "access_token": self.__token,
                "$filter": fil,
                "$expand": "images",
                "$select": "sku,images"
            }
            r = requests.request('GET', url, params=querystring)
            yield model_code, r.json()


class ChannelAdvisorTokenError(Exception):
    """Error signifying some failure with the ChannelAdvisor token."""

    pass


class ChannelAdvisorConnectionError(Exception):
    """Error signifying some failure with the ChannelAdvisor connection."""

    pass
