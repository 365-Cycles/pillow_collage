from itertools import zip_longest, groupby


def iter_slices(iterable, size=1, fill=None):
    """Iterate over slices of an iterable.

    Arguments:
        iterable {iterable} -- Iterable object.

    Keyword Arguments:
        size {int} -- Size of your slices (default: {1})
        fill {any} -- What to fill incomplete slices with (default: {None})

    Returns:
        list -- A slice of your initial list.

    """
    args = [iter(iterable)] * size
    return zip_longest(*args, fillvalue=fill)


def first_true(iterable, default=False, key=None):
    return next(filter(key, iterable), default)


def noidx_groupby(iterable, key=None):
    for i in groupby(iterable, key=key):
        yield tuple(tuple(i)[1])

