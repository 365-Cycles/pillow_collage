import csv
import math
import os
import sys
from io import BytesIO
from itertools import tee
from random import randint
from typing import Sequence

import requests
from PIL import Image
from recordclass import RecordClass

from pillow_collage.ca_api import CAApi
from pillow_collage.helpers import noidx_groupby as groupby


class Vec2(RecordClass):
    x: int = 0
    y: int = 0


class RGBA(RecordClass):
    r: int = 0
    g: int = 0
    b: int = 0
    a: int = 0


def pairwise(iterable):
    """s -> (s0, s1), (s1, s2), (s2, s3), ..."""
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def hex_color(chars=3):
    color = sum([
        val * 16 ** exp for exp, val in enumerate(
            (randint(0, 15) for z in range(chars))
        )
    ])
    inverse = sum([15 * 16 ** exp for exp in range(chars)]) - color
    return ("000000" + hex(color)[2:])[-chars:], ("000000" + hex(inverse)[2:])[-chars:]


def dynamic_collage(images: Sequence, wide: bool = False):
    def scale(image, max_size, method=Image.ANTIALIAS):
        """
        resize 'image' to 'max_size' keeping the aspect ratio
        and place it in center of white 'max_size' image
        """
        im_aspect = float(image.size[0]) / float(image.size[1])
        out_aspect = float(max_size[0]) / float(max_size[1])
        if im_aspect >= out_aspect:
            scaled = image.resize((max_size[0], int((float(max_size[0]) / im_aspect) + 0.5)), method)
        else:
            scaled = image.resize((int((float(max_size[1]) * im_aspect) + 0.5), max_size[1]), method)

        offset_ = (math.ceil((max_size[0] - scaled.size[0]) / 2), math.ceil((max_size[1] - scaled.size[1]) / 2))
        back = Image.new("RGB", max_size, "white")
        back.paste(scaled, offset_)
        return back

    if len(images) < 1:
        raise TypeError("dynamic_collage() takes 1 or more arguments, (0 provided).")
    count = len(images)
    max_dims = max([x.size[0] for x in images]), max([x.size[1] for x in images])
    if wide:
        cols = math.floor(math.sqrt(len(images)))
        rows = math.ceil(count / cols)
    else:
        cols = math.ceil(math.sqrt(len(images)))
        rows = math.ceil(count / cols)

    dims = Vec2(*images[0].size)
    bg = (255, 255, 255, 255)
    base = Image.new(
        'RGB',
        (cols * dims.x, rows * dims.y),
        bg
    )
    for y in range(rows):
        top = y * dims.y
        for x in range(cols):
            left = x * dims.x
            idx = y * cols + x
            if idx >= count:
                break
            if y < rows - 1:
                box = left, top
                base.paste(scale(images[idx], max_dims), box=box)
            else:
                offset = dims.x // (cols - count % cols) // 2
                box = left + (offset if count % cols != 0 else 0), top
                base.paste(images[idx], box=box)
    return base


imgs = {}

if __name__ == "__main__":
    with open(sys.argv[1], 'rt') as in_file:
        reader = csv.reader(in_file)
        with CAApi() as ca:
            for code, json in ca.get_products([x[0] for x in reader]):
                urls = []
                for prod in sorted(json.get('value', []), key=lambda x: x.get('Sku')):
                    if prod.get('Images', []):
                        urls.append(prod.get('Images', [])[0].get('Url'))
                    if len(urls) >= 4:
                        break
                imgs[code] = urls

    for k, v in imgs.items():
        pass

    uri = "https://picsum.photos/seed/{seed}/640/480"
    uri_2 = "https://picsum.photos/seed/{seed}/480/640"

    imgs = []
    imgs_2 = []

    for i in range(7):
        img_uri = uri.format(seed=randint(0, 9999))
        r = requests.get(
            img_uri,
            stream=True,
            allow_redirects=True,
            timeout=5
        )
        imgs.append(Image.open(BytesIO(r.content)))

    final = dynamic_collage(imgs)
    if final.size[0] >= final.size[1]:
        size = (1000, math.floor(1000 / final.size[0] * final.size[1]))
    else:
        size = (math.floor(1000 / final.size[1] * final.size[0]), 1000)
    loc = os.getcwd()
    final.resize(size, resample=Image.LANCZOS).save(loc + "/test.jpg", format="JPEG")
